# FROM ubuntu:20.04
FROM gitpod/workspace-full-vnc

RUN sudo apt update && \
 	sudo apt upgrade -y
RUN curl -fsSL https://get.docker.com | sudo sh &&\
	sudo usermod -aG docker gitpod &&\
	sudo service docker start
